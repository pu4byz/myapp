import { Component } from '@angular/core';
import { NavController, AlertController  } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

@Component({
  selector: 'page-pontos-turisticos',
  templateUrl: 'pontos-turisticos.html'
})
export class PontosTuristicosPage {

  public feeds: Array<string>;
  private url: string = "http://localhost/webservice/lista_cidades.php"; 
  items = {};

  constructor(public navCtrl: NavController, public http: Http, public alertCtrl: AlertController) {

   this.items = [
   {imagem: 'assets/img/1Uvv0KOStqeu2AHtjhYw_estaao-2.jpg', titulo: 'Estação Ferroviária'},
   {imagem: 'assets/img/cRp5dnTBRF60D6p6mvuh_casa-da-cultura-1.jpg', titulo: 'Casa da Cultura'},
   {imagem: 'assets/img/VusUAtxhTtqe3zhT1EPR_Foto-0014.jpg', titulo: 'Ponte dos Boiadeiros'},
   {imagem: 'assets/img/XIhzsMyTSztXe7Dgcw1g_abrigo-da-locomotiva.jpg', titulo: 'Abrigo da Locomotiva'},
   {imagem: 'assets/img/uj9TRdG7T8WtLFmFYutN_casa-onde-nasceu-pele.jpg', titulo: 'Museu Pelé'},
   {imagem: 'assets/img/UIMo7hnSm8LCgcMg1DAZ_TrsCoraes-pontesobreorioVerde-RMG-6-7-12.jpg', titulo: 'Ponte de Ferro'},
   {imagem: 'assets/img/kD9PLG1wQbCF0DfWlGQv_PMTC2.JPG', titulo: 'Parque Infantil'},


       
   ];
      this.http.get(this.url).map(res => res.json()).
      subscribe(data => {
        this.feeds = data.Lista;
      }); 

  }


  itemSelected(item){

    const alert = this.alertCtrl.create({
      title: item.titulo, 
      buttons: ['OK']
    });
    alert.present();

    }

}




