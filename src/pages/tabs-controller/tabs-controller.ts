import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';
//import { InicioPage } from '../inicio/inicio';
import { LoginPage } from '../login/login';
import { CadastroPage } from '../cadastro/cadastro';
import { PontosTuristicosPage } from '../pontos-turisticos/pontos-turisticos';

@Component({
  selector: 'page-tabs-controller',
  templateUrl: 'tabs-controller.html'
})
export class TabsControllerPage {

  tab1Root: any = InicioPage;
  tab2Root: any = LoginPage;
  tab3Root: any = CadastroPage;
  tab4Root: any = PontosTuristicosPage;
  constructor(public navCtrl: NavController) {
  }
  //goToInicio(params){
   // if (!params) params = {};
    //this.navCtrl.push(InicioPage);
//  }
goToLogin(params){
    if (!params) params = {};
    this.navCtrl.push(LoginPage);
  }goToCadastro(params){
    if (!params) params = {};
    this.navCtrl.push(CadastroPage);
  }goToPontosTuristicos(params){
    if (!params) params = {};
    this.navCtrl.push(PontosTuristicosPage);
  }
}
