import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { InicioPage } from '../inicio/inicio';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
login = {}
  constructor(public navCtrl: NavController, public alertCtrl:
  AlertController) {
  this.login = {} 
  }
  goToInicio(params){
    if (!params) params = {};
    this.navCtrl.push(InicioPage);
  }

  loginForm(){
  	const alert = this.alertCtrl.create({
      title: 'Aviso!',
      subTitle: 'Login efetuado com sucesso!', 
      buttons: ['OK']
    });
    alert.present();
  }
}
